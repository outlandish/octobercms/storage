<?php

namespace OutlandishIdeas\OctoberStorage;

use OutlandishIdeas\OctoberStorage\Datasource\DatasourceProxy;
use OutlandishIdeas\OctoberStorage\Datasource\DbDatasource;
use OutlandishIdeas\OctoberStorage\Model\StorageSettings;
use OutlandishIdeas\OctoberStorage\Resolver\Resolver;
use October\Rain\Halcyon\Processors\Processor;
use October\Rain\Support\ServiceProvider;

/**
 * Class StorageServiceProvider
 */
class StorageServiceProvider extends ServiceProvider
{

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            'outlandishideas.octoberstorage.datasource.proxy',
            function () {
                $enabled     = (bool)StorageSettings::get('enabled', false);
                $directories = StorageSettings::get('directories', []);
                $directories = !is_array($directories) ? [] : $directories;
                
                return new DatasourceProxy(
                    new DbDatasource(new Processor()), $directories, $enabled
                );
            }
        );

        $this->app->extend(
            'halcyon',
            function () {
                return new Resolver();
            }
        );
    }
}
