<?php

namespace OutlandishIdeas\OctoberStorage\Model;

use Model;

/**
 * Class StorageSettings
 *
 * @package OutlandishIdeas\OctoberStorage\Model
 */
class StorageSettings extends Model
{
    /**
     * @var array
     */
    public $implement = ['System.Behaviors.SettingsModel'];

    /**
     * @var string
     */
    public $settingsCode = 'outlandishideas_octoberstorage_settings';

    /**
     * @var string
     */
    public $settingsFields = 'fields.yaml';
}
