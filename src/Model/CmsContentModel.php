<?php

namespace OutlandishIdeas\OctoberStorage\Model;

use Model;

/**
 * Class CmsContentModel
 *
 * @package OutlandishIdeas\OctoberStorage\Model
 */
class CmsContentModel extends Model
{
    /**
     * @var string The database table used by the model.
     */
    public $table = 'outlandishideas_octoberstorage_cms_content';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $hasOne        = [];
    public $hasMany       = [];
    public $belongsTo     = [];
    public $belongsToMany = [];
    public $morphTo       = [];
    public $morphOne      = [];
    public $morphMany     = [];
    public $attachOne     = [];
    public $attachMany    = [];
}
