<?php

namespace OutlandishIdeas\OctoberStorage\Resolver;

use OutlandishIdeas\OctoberStorage\Datasource\DatasourceProxy;
use Illuminate\Support\Facades\App;
use October\Rain\Halcyon\Datasource\DatasourceInterface;
use October\Rain\Halcyon\Datasource\Resolver as BaseResolver;

/**
 * Class Resolver
 *
 * @package OutlandishIdeas\OctoberStorage\Resolver
 */
class Resolver extends BaseResolver
{
    public function addDatasource($name, DatasourceInterface $datasource)
    {
        /** @var DatasourceProxy $proxy */
        $proxy = App::make('outlandishideas.octoberstorage.datasource.proxy');
        $proxy->setFallbackDatasource($datasource);

        parent::addDatasource($name, $proxy);
    }

}
